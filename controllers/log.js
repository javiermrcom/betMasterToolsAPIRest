const Log = require('../models/log');

function getLogs(req, res) {
  Log.find(req.query).sort('_id').exec((err, logs) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!logs) return res.status(404).send({message: 'No existen recursos'});

    res.status(200).send({logs: logs});
  });
}

function saveLogs(req, res) {
  const logsToStore = parseLogs(req.body);
  const logsStored = [];

  logsToStore.map(log => {
    log.save((err, log) => {
      if (err) res.status(500).send({message: `Error saving data: ${err}`});
      logsStored.push(log);
    });
  });

  res.status(201).send({logs: logsStored});
}

function deleteLogs(req, res) {
  let configurationId = req.params.configurationId;

  Log.remove({configurationId: configurationId}, (err, logs) => {
    if (err) res.status(500).send({message: `Error removing logs: ${err}`});
  });
}

function parseLogs(logs) {
  let parsedLogs = [];
  let logFields = ['configurationId', 'resourceId', 'userId', 'action', 'data', 'description'];

  if (!logs.length) {
    logs = [logs];
  }

  logs.map(log => {
    let logParsed = new Log();

    logFields.map(field => {
      logParsed[field] = log[field];
    });

    parsedLogs.push(logParsed);
  });

  return parsedLogs;
}

module.exports = {
  saveLogs,
  getLogs,
  deleteLogs
};