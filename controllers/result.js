const Result = require('../models/result');

function getResult(req, res) {
  const configurationId = req.params.configurationId;

  Result.find({configurationId: configurationId}).sort('-_id').limit(1).exec((err, result) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!result) return res.status(404).send({message: 'No existen resultados'});

    res.status(200).send({result: result});
  });
}

function getResults(req, res) {
  Result.find(req.query).sort('-_id').exec((err, results) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!results) return res.status(404).send({message: 'No existen resultados'});

    res.status(200).send({results: results});
  });
}

function saveResult(req, res) {
  let result = new Result();
  let resultFields = ['userId', 'configurationId', 'resourceId', 'summary'];

  resultFields.forEach(field => {
    result[field] = req.body[field];
  });

  // TODO: Más métricas

  // TODO: si ESTÁ ACTUALIZAR Y ELIMINAR LIMIT EN GETRESULTS

  result.save((err, resultStored) => {
    if (err) res.status(500).send({message: `Error al salvar en la base de datos ${err}`});

    res.status(201).send({result: resultStored});
  })
}

function deleteResult(req, res) {
  let resultId = req.params.resultId;

  Result.findById(resultId, (err, result) => {
    if (err) res.status(500).send({message: `Error al borrar el resultado: ${err}`});
    if (!result) return res.status(404).send({message: 'El resultado no existe'});

    result.remove(err => {
      if (err) res.status(500).send({message: `Error al borrar el resultado: ${err}`});
      res.status(200).send({message: 'El resultado ha sido eliminada'});
    });
  });
}

module.exports = {
  getResult,
  getResults,
  saveResult,
  deleteResult
};