const Strategy = require('../models/strategy');

function getStrategy(req, res) {
  let strategyId = req.params.strategyId;

  Strategy.findById(strategyId, (err, strategy) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!strategy) return res.status(404).send({message: 'El recurso no existe'});

    res.status(200).send({strategy});
  })
}

function getStrategies(req, res) {
  Strategy.find(req.query).sort('-_id').exec((err, strategies) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!strategies) return res.status(404).send({message: 'No existen estrategias'});

    res.status(200).send({strategies: strategies});
  });
}

function saveStrategy(req, res) {
  let strategy = new Strategy();
  let strategyFields = ['name', 'description', 'fields'];

  strategyFields.forEach(field => {
    strategy[field] = req.body[field];
  });

  strategy.save((err, strategyStored) => {
    if (err) res.status(500).send({message: `Error al salvar en la base de datos ${err}`});

    res.status(201).send({strategy: strategyStored});
  })
}

module.exports = {
  getStrategy,
  getStrategies,
  saveStrategy
};