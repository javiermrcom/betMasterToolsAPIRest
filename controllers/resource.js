const Resource = require('../models/resource');

function getResource(req, res) {
  let resourceId = req.params.resourceId;

  Resource.findById(resourceId, (err, resource) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!resource) return res.status(404).send({message: 'El recurso no existe'});

    res.status(200).send({resource: resource});
  })
}

function getResources(req, res) {
  Resource.find(req.query).sort('-_id').exec((err, resources) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!resources) return res.status(404).send({message: 'No existen recursos'});

    res.status(200).send({resources: resources});
  });
}

function saveResource(req, res) {
  let resource = new Resource();
  let resourceFields = ['date', 'count', 'userId', 'name'];

  resourceFields.forEach(field => {
    resource[field] = req.body[field];
  });

  resource.save((err, resourceStored) => {
    if (err) res.status(500).send({message: `Error al salvar en la base de datos ${err}`});

    res.status(201).send({resource: resourceStored});
  })
}

function updateResource(req, res) {
  let resourceId = req.params.resourceId;
  let update = req.body;

  Resource.findByIdAndUpdate(resourceId, update, (err, resourceUpdated) => {
    if (err) res.status(500).send({message: `Error updating resource: ${err}`});

    res.status(200).send({resource: resourceUpdated});
  });
}

function deleteResource(req, res) {
  let resourceId = req.params.resourceId;

  Resource.findById(resourceId, (err, resource) => {
    if (err) res.status(500).send({message: `Error al borrar el recurso: ${err}`});
    if (!resource) return res.status(404).send({message: 'El recurso no existe'});

    resource.remove(err => {
      if (err) res.status(500).send({message: `Error al borrar el recurso: ${err}`});
      res.status(200).send({message: 'El recurso ha sido eliminada'});
    });
  });
}

module.exports = {
  getResource,
  getResources,
  saveResource,
  updateResource,
  deleteResource
};