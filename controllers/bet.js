const Bet = require('../models/bet');

function getBet(req, res) {
  let betId = req.params.betId;

  Bet.findById(betId, (err, bet) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!bet) return res.status(404).send({message: 'La apuesta no existe'});

    res.status(200).send({bet});
  })
}

function getBets(req, res) {

  // Search by team
  if (req.query.team) {
    req.query['$or'] = [{homeTeam: req.query.team}, {awayTeam: req.query.team}];
    delete req.query.team;
  }

  Bet.find(req.query, (err, bets) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!bets) return res.status(404).send({message: 'No existen apuestas'});

    res.status(200).send({
      bets: bets,
      count: bets.length
    });
  });
}

function saveBets(req, res) {
  const betsToStore = parseBets(req.body);
  const betsStored = [];

  betsToStore.map(bet => {
    bet.save((err, bet) => {
      if (err) res.status(500).send({message: `Error saving data: ${err}`});
      betsStored.push(bet);
    });
  });

  res.status(201).send({bets: betsStored});
}

function parseBets(bets) {
  let parsedBets = [];
  let betFields = ['resourceId', 'div', 'date', 'homeTeam', 'awayTeam', 'fthg', 'ftag', 'ftr', 'hthg', 'htag', 'htr', 'bh', 'bd', 'ba'];

  if (!bets.length) {
    bets = [bets];
  }

  bets.map(bet => {
    let betParsed = new Bet();

    betFields.map(field => {
      betParsed[field] = bet[field];
    });

    parsedBets.push(betParsed);
  });

  return parsedBets;
}


function updateBet(req, res) {
  let betId = req.params.betId;
  let update = req.body;

  Bet.findByIdAndUpdate(betId, update, (err, betUpdated) => {
    if (err) res.status(500).send({message: `Error al actualizar la apuesta: ${err}`});

    res.status(200).send({bet: betUpdated});
  });
}

function deleteBet(req, res) {
  let betId = req.params.betId;

  Bet.findById(betId, (err, bet) => {
    if (err) res.status(500).send({message: `Error al borrar la apuesta: ${err}`});
    if (!bet) return res.status(404).send({message: 'La apuesta no existe'});

    bet.remove(err => {
      if (err) res.status(500).send({message: `Error al borrar la apuesta: ${err}`});
      res.status(200).send({message: 'La apuesta ha sido eliminada'});
    });
  });
}

module.exports = {
  getBet,
  getBets,
  saveBets,
  updateBet,
  deleteBet,
};