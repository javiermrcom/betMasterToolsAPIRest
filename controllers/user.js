const User = require('../models/user');
const service = require('../services');

function signUp(req, res) {
  const emailRegexValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const passwordRegexValidation = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,10}$/;

  if (!req.body.email.match(emailRegexValidation)) {
    return res.status(400).send({message: 'Incorrect email format', param: 'email'});
  }

  if (!req.body.password.match(passwordRegexValidation)) {
    return res.status(400).send({message: 'Incorrect password format', param: 'password'});
  }

  const user = new User({
    email: req.body.email,
    displayName: req.body.displayName,
    password: req.body.password
  });

  User.find({email: user.email}, (err, userStored) => {
    if (err) return res.status(500).send({message: 'There was an error checking email'});
    if (userStored.length) return res.status(400).send({message: 'Email used', param: 'email'});

    user.save(err => {
      if (err) return res.status(500).send({message: `Error creating user: ${err}`});

      return res.status(201).send({token: service.createToken(user)})
    })
  });
}

function signIn(req, res) {
  User.find({email: req.body.email, password: req.body.password}, (err, user) => {
    if (err) return res.status(500).send({message: err});
    if (!user || !user.length) return res.status(404).send({message: 'Incorrect login'});

    req.user = user;
    res.status(200).send({
      id: user[0]._id,
      token: service.createToken(user),
      displayName: user[0].displayName,
      email: user[0].email
    })
  });
}

function getUsers(req, res) {
  User.find({}, (err, users) => {
    if (err) return res.status(500).send({message: `Error making request ${err}`});
    if (!users) return res.status(404).send({message: 'The are not users'});

    res.status(200).send({users});
  });
}

module.exports = {
  signUp,
  signIn,
  getUsers
};