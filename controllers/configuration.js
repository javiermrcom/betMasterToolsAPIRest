const Configuration = require('../models/configuration');

function getConfiguration(req, res) {
  let configurationId = req.params.configurationId;

  Configuration.findById(configurationId, (err, configuration) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!configuration) return res.status(404).send({message: 'La configuración no existe'});

    res.status(200).send({configuration: configuration});
  })
}

function getConfigurations(req, res) {
  Configuration.find(req.query).sort('-_id').exec((err, configurations) => {
    if (err) return res.status(500).send({message: `Error al realizar la petición ${err}`});
    if (!configurations) return res.status(404).send({message: 'No existen configuraciones'});

    res.status(200).send({configurations: configurations});
  });
}

function saveConfiguration(req, res) {
  let configuration = new Configuration();
  let configurationFields = ['userId', 'strategyId', 'resourceId', 'name',  'fields', 'date', 'status'];

  for (let field of configurationFields) {
    configuration[field] = req.body[field];
  }

  configuration.save((err, configurationStored) => {
    if (err) res.status(500).send({message: `Error al salvar en la base de datos ${err}`});

    res.status(201).send({configuration: configurationStored});
  })
}

function updateConfiguration(req, res) {
  let configurationId = req.params.configurationId;
  let update = req.body;

  Configuration.findByIdAndUpdate(configurationId, update, (err, configurationUpdated) => {
    if (err) res.status(500).send({message: `Error al actualizar la configuración: ${err}`});

    res.status(200).send({configuration: configurationUpdated});
  });
}

function deleteConfiguration(req, res) {
  let configurationId = req.params.configurationId;

  Configuration.findById(configurationId, (err, configuration) => {
    if (err) res.status(500).send({message: `Error finding Configuration: ${err}`});
    if (!configuration) return res.status(404).send({message: 'Configuration not exists'});

    configuration.remove(err => {
      if (err) res.status(500).send({message: `Error deleting Configuration: ${err}`});
      res.status(200).send({message: 'Configuration deleted'});
    });
  });
}

module.exports = {
  getConfiguration,
  getConfigurations,
  saveConfiguration,
  updateConfiguration,
  deleteConfiguration
};