const express = require('express');
const multer = require('multer');
const BetCtrl = require('../controllers/bet');
const ResourceCtrl = require('../controllers/resource');
const StrategyCtrl = require('../controllers/strategy');
const ConfigurationCtrl = require('../controllers/configuration');
const ResultCtrl = require('../controllers/result');
const UserCtrl = require('../controllers/user');
const LogCtrl = require('../controllers/log');

const auth = require('../middlewares/auth');
const api = express.Router();

// -------------
// BETS
api.get('/bet', BetCtrl.getBets);
api.get('/bet/:betId', BetCtrl.getBet);
api.post('/bet', BetCtrl.saveBets);
api.put('/bet/:betId', BetCtrl.updateBet);
api.delete('/bet/:betId', BetCtrl.deleteBet);
// -------------
// RESOURCE
api.get('/resource', ResourceCtrl.getResources);
api.get('/resource/:resourceId', ResourceCtrl.getResource);
api.post('/resource', ResourceCtrl.saveResource);
api.put('/resource/:resourceId', ResourceCtrl.updateResource);
// -------------
// STRATEGIES
api.get('/strategy', StrategyCtrl.getStrategies);
api.get('/strategy/:strategyId', StrategyCtrl.getStrategy);
api.post('/strategy', StrategyCtrl.saveStrategy);
// -------------
// CONFIGURATION
api.get('/configuration', ConfigurationCtrl.getConfigurations); // after: filter by strategyId
api.get('/configuration/:configurationId', ConfigurationCtrl.getConfiguration);
/**
 * strategyId
 * params
 */
api.post('/configuration', ConfigurationCtrl.saveConfiguration);
/**
 * configurationId
 * params
 */
api.put('/configuration/:configurationId', ConfigurationCtrl.updateConfiguration);
api.delete('/configuration/:configurationId', ConfigurationCtrl.deleteConfiguration);
// -------------
// RESULT
api.get('/result/:configurationId', ResultCtrl.getResult);
/**
 * configurationId
 * resourceId
 */
api.get('/result', ResultCtrl.getResults);
api.post('/result', ResultCtrl.saveResult);
// -------------
// LOG
api.get('/log', LogCtrl.getLogs);
api.post('/log', LogCtrl.saveLogs);
api.delete('/log/:configurationId', LogCtrl.deleteLogs);

// -------------
// USER
// api.post('/user/login', UserCtrl.login);
api.get('/private', auth, (req, res) => {
  res.status(200).send({message: 'Tienes acceso'});
});
api.post('/signup', UserCtrl.signUp);
api.post('/signin', UserCtrl.signIn);
api.get('/user', UserCtrl.getUsers);

module.exports = api;