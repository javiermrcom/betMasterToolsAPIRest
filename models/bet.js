const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BetSchema = Schema({
  resourceId: String,
  div: String,
  date: Date,
  homeTeam: String,
  awayTeam: String,
  fthg: Number,
  ftag: Number,
  ftr: {
    type: String,
    enum: ['H', 'D', 'A']
  },
  hthg: Number,
  htag: Number,
  htr: {
    type: String,
    enum: ['H', 'D', 'A']
  },
  bh: Number,
  bd: Number,
  ba: Number
});

module.exports = mongoose.model('Bet', BetSchema);