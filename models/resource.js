const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ResourceSchema = Schema({
  date: String, // creation date
  count: Number, // bets count,
  userId: String,
  name: String
});

module.exports = mongoose.model('Resource', ResourceSchema);