const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StrategySchema = Schema({
  name: String,
  description: String,
  fields: Array
});

/**
 * MARTINGALA
 * -----------------
 * team
 * result { WIN, DEADHEAT, LOSE }
 * minBet
 * unit
 * startBet
 * stopBet
 */

module.exports = mongoose.model('Strategy', StrategySchema);