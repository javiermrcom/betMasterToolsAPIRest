const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ResultSchema = Schema({
  userId: String,
  configurationId: String,
  resourceId: String,
  summary: {
    earned: Number,
    lost: Number,
    balance: Number
  }


  // TODO: ADD DATE
  // TODO: ANALYTIC DATA (IMPORTANT!!)
});

module.exports = mongoose.model('Result', ResultSchema);