const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');

const LogSchema = new Schema({
  userId: String,
  configurationId: String,
  resourceId: String,
  action: String,
  data: Object,
  description: String
});

module.exports = mongoose.model('Log', LogSchema);