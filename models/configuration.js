const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigurationSchema = Schema({
  userId: String,
  strategyId: String,
  resourceId: String,
  name: String,
  fields: Object,// strategy params
  date: Date,
  status: String
});

module.exports = mongoose.model('Configuration', ConfigurationSchema);