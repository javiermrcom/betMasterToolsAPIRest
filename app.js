const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const api = require('./routes');
const cors = require('cors');

// parse application/x-www-form-urlencoded
app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));
// parse application/json
app.use(bodyParser.json());

//create a cors middleware
app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.header('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.header('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use('/api', api);

// use it before all route definitions
// app.use(cors({origin: 'http://localhost:4200'}));
// app.use(cors({origin: 'http://localhost:9876'}));

module.exports = app;