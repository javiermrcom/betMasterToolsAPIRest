const mongoose = require('mongoose');
const app = require('./app');
const config = require('./config');

mongoose.connect(config.db, (err, res) => {
  if (err) {
    return console.log(`Error al conectar a la base de datos: ${err}`);
  }

  app.on('listening', function () {
    console.log('OK, server is running');
  });

  console.log('Connected to database ...');

  app.listen(config.port, () => {
    console.log(`API REST running in http://localhost:${config.port}`);
  });
});